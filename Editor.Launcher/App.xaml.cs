﻿using Lib.Shared;
using System.Globalization;
using System.Windows;
using System.Windows.Threading;

namespace Editor.Launcher
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;

            Current.DispatcherUnhandledException += UnhandledException;                        
        }

        private void UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Output.TraceError(e.Exception);
        }
    }
}
