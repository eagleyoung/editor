﻿using Editor.Launcher.Editors;
using Lib.Shared;
using Lib.WPF;

namespace Editor.Launcher.Windows
{
    public class MainWindowModel : BaseViewModel
    {
        /// <summary>
        /// Список доступных редакторов
        /// </summary>
        public SafeObservableCollection<BaseEditorItem> Editors { get; } = SafeObservableCollection.Create<BaseEditorItem>();

        public MainWindowModel()
        {
            Editors.Add(new TreePeopleEditor());
            Editors.Add(new WebEditor());
        }

        BaseEditorItem selectedEditor;
        /// <summary>
        /// Выбранный редактор
        /// </summary>
        public BaseEditorItem SelectedEditor
        {
            get
            {
                return selectedEditor;
            }
            set
            {
                selectedEditor = value;

                if (selectedEditor == null) return;

                if (!selectedEditor.IsConfigured)
                {
                    Output.ErrorAdditional("Настройте редактор перед запуском (см. информацию)", 
                        "Для настройки используйте конфигурационные файлы в папке /cfg. Каждый редактор имеет свой конфигурационный файл.");
                    return;
                }

                selectedEditor.Start();
                OnRequestClose();
            }
        }
    }
}
