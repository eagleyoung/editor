﻿using Lib.Core;
using Lib.WebClient;

namespace Editor.Launcher.Editors
{
    public class WebEditor : BaseEditorItem
    {
        public WebEditor() : base(
            "Web",
            "Web",
            "Редактор данных веб-сервера"
            ) { }

        public override bool IsConfigured {
            get
            {
                var server = Config.Instance.ByKey("Web.Server")?.Value;
                var port = Config.Instance.ByKey("Web.Port")?.Value;
                return server != null & port != null;
            }
        }

        public override void Start()
        {
            var server = Config.Instance.ByKey("Web.Server")?.Value;
            var port = Config.Instance.ByKey("Web.Port")?.Value;

            BaseManager.Load(server, port);

            new Web.Windows.MainWindow().Show();
        }
    }
}
