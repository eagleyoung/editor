﻿using Editor.Core.Data;
using Lib.Core;

namespace Editor.Launcher.Editors
{
    public class TreePeopleEditor : BaseEditorItem
    {
        public TreePeopleEditor() : base(
            "TreePeople", 
            "TreePeople", 
            "Редактор игры TreePeople"
            ) { }

        public override bool IsConfigured => !string.IsNullOrEmpty(Config.Instance.ByKey("TreePeople.DataPath")?.Value);

        public override void Start()
        {
            DataHandler.SetPath(Config.Instance.ByKey("TreePeople.DataPath").Value);
            new TreePeople.Windows.MainWindow().Show();
        }
    }
}
