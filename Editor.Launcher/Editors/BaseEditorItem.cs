﻿using Lib.WPF;

namespace Editor.Launcher.Editors
{
    /// <summary>
    /// Отдельный редактор
    /// </summary>
    public abstract class BaseEditorItem : NotifyObject
    {
        /// <summary>
        /// Ключ редактора
        /// </summary>
        public string Key { get; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Путь до иконки
        /// </summary>
        public string IconPath
        {
            get
            {
                return $"/Images/{Key}.png";
            }
        }

        public BaseEditorItem(string key, string name, string description)
        {
            Key = key;
            Name = name;
            Description = description;
        }

        /// <summary>
        /// Редактор настроен
        /// </summary>
        public abstract bool IsConfigured { get; }

        /// <summary>
        /// Запустить редактор
        /// </summary>
        public abstract void Start();
    }
}
