﻿using Lib.WebClient;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Editor.Web.Managers
{
    /// <summary>
    /// Менеджер событий
    /// </summary>
    public class EventManager : BaseManager
    {
        protected override string RoutePrefix => "events";

        static Lazy<EventManager> instance = new Lazy<EventManager>(() => new EventManager(), true);
        public static EventManager Instance => instance.Value;

        /// <summary>
        /// Получить все события
        /// </summary>
        public async Task<List<Event>> GetAll()
        {
            if (client == null) return null;

            var response = await client.GetAsync(Route());
            if (!response.IsSuccessStatusCode)
            {
                await response.OutputResponseErrorAsync();
                return null;
            }

            return await response.Content.ReadAsAsync<List<Event>>();
        }

        /// <summary>
        /// Получить события для определенного пользователя
        /// </summary>
        public async Task<List<Event>> GetForUser(string userToken)
        {
            if (client == null) return null;

            var response = await client.GetAsync(Route($"user/{userToken}"));
            if (!response.IsSuccessStatusCode)
            {
                await response.OutputResponseErrorAsync();
                return null;
            }

            return await response.Content.ReadAsAsync<List<Event>>();
        }

        /// <summary>
        /// Добавить новое событие
        /// </summary>
        public async Task<bool> Create(Event ev)
        {
            if (client == null) return false;

            var response = await client.PostAsJsonAsync(Route(), ev);
            if (!response.IsSuccessStatusCode)
            {
                await response.OutputResponseErrorAsync();
                return false;
            }

            var id = await response.Content.ReadAsAsync<long>();
            ev.Id = id;
            return true;
        }

        /// <summary>
        /// Обновить событие
        /// </summary>
        public async Task<bool> Update(Event ev)
        {
            if (client == null) return false;

            var response = await client.PutAsJsonAsync(Route($"{ev.Id}"), ev);

            if (!response.IsSuccessStatusCode)
            {
                await response.OutputResponseErrorAsync();
                return false;
            }

            var token = await response.Content.ReadAsAsync<long>();
            ev.UpdateToken = token;

            return true;
        }

        /// <summary>
        /// Удалить событие
        /// </summary>
        public async Task<bool> Delete(Event ev)
        {
            if (client == null) return false;

            var response = await client.DeleteAsync(Route($"{ev.Id}"));

            if (!response.IsSuccessStatusCode)
            {
                await response.OutputResponseErrorAsync();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Событие было просмотрено
        /// </summary>
        public async Task<bool> EventShown(string userToken, long eventId)
        {
            if (client == null) return false;

            var response = await client.GetAsync(Route($"user/{userToken}/{eventId}"));

            if (!response.IsSuccessStatusCode)
            {
                await response.OutputResponseErrorAsync();
                return false;
            }

            return true;
        }
    }
}
