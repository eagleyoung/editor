﻿namespace Editor.Web.Managers
{
    /// <summary>
    /// Событие
    /// </summary>
    public class Event : TraceableManagerItem
    {
        Loc title;
        /// <summary>
        /// Наименование события
        /// </summary>
        public Loc Title {
            get
            {
                return title;
            }
            set
            {
                if(title != value)
                {
                    title = value;
                    OnPropertyChanged();
                }
            }
        }

        Loc description;
        /// <summary>
        /// Перевод события
        /// </summary>
        public Loc Description {
            get
            {
                return description;
            }
            set
            {
                if(description != value)
                {
                    description = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
