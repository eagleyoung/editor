﻿using Lib.WPF;

namespace Editor.Web.Managers
{
    /// <summary>
    /// Отдельный перевод
    /// </summary>
    public class Loc : NotifyObject
    {
        string en;
        /// <summary>
        /// Английский перевод
        /// </summary>
        public string En {
            get
            {
                return en;
            }
            set
            {
                if(en != value)
                {
                    en = value;
                    OnPropertyChanged();
                }
            }
        }

        string ru;
        /// <summary>
        /// Русский перевод
        /// </summary>
        public string Ru {
            get
            {
                return ru;
            }
            set
            {
                if(ru != value)
                {
                    ru = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
