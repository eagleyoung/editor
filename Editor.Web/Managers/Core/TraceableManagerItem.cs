﻿using Lib.WebClient;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Editor.Web.Managers
{
    /// <summary>
    /// Объект менеджера данных с возможностью рассылки состояния
    /// </summary>
    public class TraceableManagerItem : BaseManagerItem, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Параметр объекта был изменен
        /// </summary>
        protected virtual void OnPropertyChanged([CallerMemberName] string key = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(key));
        }
    }
}
