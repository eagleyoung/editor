﻿using Lib.WPF;

namespace Editor.TreePeople.Controls
{
    /// <summary>
    /// Отдельный элемент
    /// </summary>
    public class LevelKeyItem : NotifyObject
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public string Key { get; }

        bool enabled;
        /// <summary>
        /// Элемент включен?
        /// </summary>
        public bool Enabled {
            get
            {
                return enabled;
            }
            set
            {
                if(enabled != value)
                {
                    enabled = value;
                    OnPropertyChanged();
                }
            }
        }

        public LevelKeyItem(string key, bool enabled)
        {
            Key = key;
            this.enabled = enabled;
        }
    }
}
