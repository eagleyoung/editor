﻿using Lib.WPF;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Editor.TreePeople.Controls
{
    /// <summary>
    /// Коллекция элементов
    /// </summary>
    public class LevelKeyCollection : SafeObservableCollection<LevelKeyItem>
    {
        protected override void InsertItem(int index, LevelKeyItem item)
        {
            base.InsertItem(index, item);
            item.PropertyChanged += TraceChange;
        }

        public override void AddRange(IEnumerable<LevelKeyItem> items)
        {
            base.AddRange(items);
            foreach(var item in items)
            {
                item.PropertyChanged += TraceChange;
            }
        }

        private void TraceChange(object sender, PropertyChangedEventArgs e)
        {
            Changed();
        }

        /// <summary>
        /// Коллекция изменилась
        /// </summary>
        public event Action Changed = delegate { };

        ~LevelKeyCollection()
        {
            foreach(var item in this)
            {
                item.PropertyChanged -= TraceChange;
            }
        }
    }

    public static class LevelKeyCollectionFactory {

        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static LevelKeyCollection Create()
        {
            LevelKeyCollection output = null;
            new Action(delegate {
                output = new LevelKeyCollection();
            }).UIInvoke();
            return output;
        }
    }
}
