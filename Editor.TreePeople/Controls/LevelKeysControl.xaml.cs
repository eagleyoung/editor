﻿using Editor.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Editor.TreePeople.Controls
{
    /// <summary>
    /// Логика взаимодействия для LevelKeysControl.xaml
    /// </summary>
    public partial class LevelKeysControl : UserControl
    {
        public static readonly DependencyProperty LevelPackProperty =
            DependencyProperty.Register("LevelPack", typeof(ICollection<string>), typeof(LevelKeysControl), new UIPropertyMetadata(null));        

        public ICollection<string> LevelPack
        {
            get
            {
                return (ICollection<string>)GetValue(LevelPackProperty);
            }
            set
            {
                SetValue(LevelPackProperty, value);
            }
        }

        public LevelKeysControl()
        {
            Levels.Changed += Changed;
            InitializeComponent();

            var descriptor = DependencyPropertyDescriptor.FromProperty(LevelPackProperty, typeof(LevelKeysControl));
            descriptor.AddValueChanged(this, LoadLevels);
        }

        void LoadLevels(object sender, EventArgs e)
        {
            Levels.Clear();
            List<LevelKeyItem> keys = new List<LevelKeyItem>();
            foreach (var level in LevelManager.GetInstance().Items)
            {
                keys.Add(new LevelKeyItem(level.Key, LevelPack.Contains(level.Key)));
            }
            Levels.AddRange(keys);
        }

        ~LevelKeysControl()
        {
            Levels.Changed -= Changed;
            var descriptor = DependencyPropertyDescriptor.FromProperty(LevelPackProperty, typeof(LevelKeysControl));
            descriptor.RemoveValueChanged(this, LoadLevels);
        }

        private void Changed()
        {
            if(LevelPack != null)
            {
                LevelPack.Clear();
                foreach(var item in Levels)
                {
                    if (item.Enabled) LevelPack.Add(item.Key);
                }
            }
        }

        /// <summary>
        /// Список уровней
        /// </summary>
        public LevelKeyCollection Levels { get; } = LevelKeyCollectionFactory.Create();
    }
}
