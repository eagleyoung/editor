﻿using Editor.Core;
using Editor.Core.Data;
using Editor.TreePeople.Data;
using System.Windows;

namespace Editor.TreePeople.Windows
{
    public class MainWindowModel : EditorViewModel
    {
        /// <summary>
        /// Менеджер уровней игры
        /// </summary>
        public LevelManager Level { get; } = LevelManager.GetInstance();

        /// <summary>
        /// Менеджер опыта
        /// </summary>
        public ExpManager Exp { get; } = ExpManager.GetInstance();

        /// <summary>
        /// Менеджер улучшений
        /// </summary>
        public UpgradeManager Upgrade { get; } = UpgradeManager.GetInstance();

        /// <summary>
        /// Менеджер врагов
        /// </summary>
        public EnemyManager Enemy { get; } = EnemyManager.GetInstance();

        /// <summary>
        /// Менеджер событий дня
        /// </summary>
        public DayEventManager DayEvent { get; } = DayEventManager.GetInstance();

        /// <summary>
        /// Менеджер героев
        /// </summary>
        public HeroManager Hero { get; } = HeroManager.GetInstance();

        /// <summary>
        /// Менеджер погоды
        /// </summary>
        public WeatherManager Weather { get; } = WeatherManager.GetInstance();

        FrameworkElement selectedTab;
        /// <summary>
        /// Выбранная закладка
        /// </summary>
        public FrameworkElement SelectedTab {
            get
            {
                return selectedTab;
            }
            set
            {
                selectedTab = value;
                OnPropertyChanged();
            }
        }

        protected override void Search(string text)
        {
            base.Search(text);
            if(SelectedTab != null)
            {
                var manager = SelectedTab.DataContext as IDataManager;
                if(manager != null)
                {
                    manager.Search(text);
                }
            }
        }
    } 
}
