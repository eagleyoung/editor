﻿using Editor.Core.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Менеджер врагов
    /// </summary>
    public class EnemyManager : BaseDataManager<EnemyData>
    {
        static Lazy<EnemyManager> instance = new Lazy<EnemyManager>(()=> BaseDataManager.Create<EnemyManager, EnemyData>("Enemy"), true);

        public override string Name => "Враги";

        public static EnemyManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Enemy");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            ExportHandler.Save("Enemy", state.ToString(Formatting.Indented));
        }

        protected override void ProceedAdd(EnemyData item)
        {
            base.ProceedAdd(item);
            item.Frequency = 1f;

            if (Items.Count == 0)
            {
                item.Key = "0";
                return;
            }

            var last = Items[Items.Count - 1];
            if (int.TryParse(last.Key, out int lastIndex))
            {
                item.Key = (lastIndex + 1).ToString();
            }
        }
    }
}
