﻿using System.Collections.Generic;
using DataInterface.Core;
using DataInterface.TreePeople;
using Editor.Core.Data;
using Lib.WPF;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Враг
    /// </summary>
    public class EnemyData : LocalizedDataObject, IEnemyData
    {
        string conditions;
        /// <summary>
        /// Условия появления врага
        /// </summary>
        public string Conditions {
            get
            {
                return conditions;
            }
            set
            {
                if(conditions != value)
                {
                    conditions = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Список ключей уровней, на которых доступен этот враг
        /// </summary>
        public SafeObservableCollection<string> LevelKeys { get; set; } = SafeObservableCollection.Create<string>();
        IEnumerable<string> IEnemyData.LevelKeys => LevelKeys;

        double minAttack;
        /// <summary>
        /// Минимальная атака
        /// </summary>
        public double MinAttack {
            get
            {
                return minAttack;
            }
            set
            {
                if(minAttack != value)
                {
                    minAttack = value;
                    OnPropertyChanged();

                    if(MaxAttack < double.Epsilon)
                    {
                        MaxAttack = value;
                    }
                }
            }
        }

        double maxAttack;
        /// <summary>
        /// Максимальная атака
        /// </summary>
        public double MaxAttack
        {
            get
            {
                return maxAttack;
            }
            set
            {
                if(maxAttack != value)
                {
                    maxAttack = value;
                    OnPropertyChanged();
                }
            }
        }

        double minDefence;
        /// <summary>
        /// Минимальная защита
        /// </summary>
        public double MinDefence {
            get
            {
                return minDefence;
            }
            set
            {
                if(minDefence != value)
                {
                    minDefence = value;
                    OnPropertyChanged();

                    if(MaxDefence < double.Epsilon)
                    {
                        MaxDefence = value;
                    }
                }
            }
        }

        double maxDefence;
        /// <summary>
        /// Максимальная защита
        /// </summary>
        public double MaxDefence
        {
            get
            {
                return maxDefence;
            }
            set
            {
                if(maxDefence != value)
                {
                    maxDefence = value;
                    OnPropertyChanged();
                }
            }
        }

        double minCount;
        /// <summary>
        /// Минимальное количество врагов
        /// </summary>
        public double MinCount {
            get
            {
                return minCount;
            }
            set
            {
                if(minCount != value)
                {
                    minCount = value;
                    OnPropertyChanged();

                    if(MaxCount < double.Epsilon)
                    {
                        MaxCount = value;
                    }
                }
            }
        }

        double maxCount;
        /// <summary>
        /// Максимальное количество
        /// </summary>
        public double MaxCount
        {
            get
            {
                return maxCount;
            }
            set
            {
                if(maxCount != value)
                {
                    maxCount = value;
                    OnPropertyChanged();
                }
            }
        }

        float frequency;
        /// <summary>
        /// Вероятность появления врага
        /// </summary>
        public float Frequency {
            get
            {
                return frequency;
            }
            set
            {
                if(frequency != value)
                {
                    frequency = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Список изменений при победе над врагом
        /// </summary>
        public DataChangeCollection Changes { get; set; } = DataChangeCollectionFactory.Create();
        IEnumerable<IDataChange> IEnemyData.Changes => Changes;

        protected override string LocKey => $"Enemy.{Key}";

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();

            pack.Add(Key);
            pack.Add(Conditions);
            pack.Add(Locs.En);
            pack.Add(Locs.Ru);

            return pack;
        }
    }
}
