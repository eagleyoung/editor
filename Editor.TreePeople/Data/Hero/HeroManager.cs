﻿using Editor.Core.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Менеджер героев
    /// </summary>
    public class HeroManager : BaseDataManager<HeroData>
    {
        static Lazy<HeroManager> instance = new Lazy<HeroManager>(()=> BaseDataManager.Create<HeroManager,HeroData>("Hero"), true);

        public override string Name => "Герои";

        public static HeroManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Hero");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            ExportHandler.Save("Hero", state.ToString(Formatting.Indented));
        }
    }
}
