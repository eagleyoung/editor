﻿using DataInterface.TreePeople;
using Editor.Core.Data;
using Lib.WPF;
using Newtonsoft.Json;
using System;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Герой
    /// </summary>
    public class HeroData : LocalizedDataObject, IHeroData
    {
        float food;
        /// <summary>
        /// Множитель сбора еды
        /// </summary>
        public float Food {
            get
            {
                return food;
            }
            set
            {
                if(food != value)
                {
                    food = value;
                    OnPropertyChanged();
                }
            }
        }

        float population;
        /// <summary>
        /// Множитель населения
        /// </summary>
        public float Population {
            get
            {
                return population;
            }
            set
            {
                if(population != value)
                {
                    population = value;
                    OnPropertyChanged();
                }
            }
        }

        float tree;
        /// <summary>
        /// Множитель строительства дерева
        /// </summary>
        public float Tree {
            get
            {
                return tree;
            }
            set
            {
                if(tree != value)
                {
                    tree = value;
                    OnPropertyChanged();
                }
            }
        }

        float army;
        /// <summary>
        /// Множитель армии
        /// </summary>
        public float Army {
            get
            {
                return army;
            }
            set
            {
                if(army != value)
                {
                    army = value;
                    OnPropertyChanged();
                }
            }
        }

        float gems;
        /// <summary>
        /// Множитель сбора кристаллов
        /// </summary>
        public float Gems {
            get
            {
                return gems;
            }
            set
            {
                if(gems != value)
                {
                    gems = value;
                    OnPropertyChanged();
                }
            }
        }

        protected override string LocKey => $"Hero.{Key}";

        Lazy<LocData> locDescription;
        [JsonIgnore]
        /// <summary>
        /// Перевод описания объекта
        /// </summary>
        public LocData LocDescription
        {
            get
            {
                return locDescription.Value;
            }
        }

        public override string Key
        {
            get => base.Key;
            set
            {
                base.Key = value;
                LocDescription.Key = $"Hero.{Key}.Description";
            }
        }

        public HeroData()
        {
            locDescription = new Lazy<LocData>(() => LocManager.Get($"Hero.{Key}.Description"));
        }

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();
            pack.Add(Key);
            pack.Add(Locs.En);
            pack.Add(Locs.Ru);
            return pack;
        }
    }
}
