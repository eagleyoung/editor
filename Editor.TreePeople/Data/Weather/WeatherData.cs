﻿using System.Collections.Generic;
using DataInterface.TreePeople;
using Editor.Core.Data;
using Lib.WPF;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Данные о погоде
    /// </summary>
    public class WeatherData : BaseData, IWeatherData
    {
        /// <summary>
        /// Список ключей уровней, на которых доступен этот тип погоды
        /// </summary>
        public SafeObservableCollection<string> LevelKeys { get; set; } = SafeObservableCollection.Create<string>();
        IEnumerable<string> IWeatherData.LevelKeys => LevelKeys;

        float frequency;
        /// <summary>
        /// Вероятность появления события
        /// </summary>
        public float Frequency {
            get
            {
                return frequency;
            }
            set
            {
                if(frequency != value)
                {
                    frequency = value;
                    OnPropertyChanged();
                }
            }
        }

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();
            pack.Add(Key);
            return pack;
        }
    }
}
