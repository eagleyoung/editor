﻿using Editor.Core.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Менеджер данных о погоде
    /// </summary>
    public class WeatherManager : BaseDataManager<WeatherData>
    {
        static Lazy<WeatherManager> instance = new Lazy<WeatherManager>(()=> BaseDataManager.Create<WeatherManager,WeatherData>("Weather"), true);

        public override string Name => "Погода";

        public static WeatherManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Weather");
        }

        protected override void ProceedAdd(WeatherData item)
        {
            base.ProceedAdd(item);
            item.Frequency = 1f;
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            ExportHandler.Save("Weather", state.ToString(Formatting.Indented));
        }
    }
}
