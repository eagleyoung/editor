﻿using System.Collections.Generic;
using DataInterface.Core;
using DataInterface.TreePeople;
using Editor.Core.Data;
using Lib.WPF;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Событие дня
    /// </summary>
    public class DayEventData : LocalizedDataObject, IDayEventData
    {
        string group;
        /// <summary>
        /// Группа событий
        /// </summary>
        public string Group {
            get
            {
                if (!string.IsNullOrEmpty(basedOn))
                {
                    try
                    {
                        return DayEventManager.Get(basedOn)?.Group;
                    }
                    catch { }
                }
                return group;
            }
            set
            {
                if (group != value)
                {
                    group = value;
                    OnPropertyChanged();
                }
            }
        }

        string conditions;
        /// <summary>
        /// Условия доступности события
        /// </summary>
        public string Conditions {
            get
            {
                if (!string.IsNullOrEmpty(basedOn))
                {
                    try
                    {
                        return DayEventManager.Get(basedOn)?.Conditions;
                    }
                    catch { }
                }
                return conditions;
            }
            set
            {
                if (conditions != value)
                {
                    conditions = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Список ключей уровней, на которых доступно это событие
        /// </summary>
        public SafeObservableCollection<string> LevelKeys { get; set; } = SafeObservableCollection.Create<string>();
        IEnumerable<string> IDayEventData.LevelKeys => LevelKeys;

        float frequency;
        /// <summary>
        /// Вероятность появления события
        /// </summary>
        public float Frequency {
            get
            {
                if (!string.IsNullOrEmpty(basedOn))
                {
                    try
                    {
                        return DayEventManager.Get(basedOn)?.Frequency ?? 1f;
                    }
                    catch { }
                }
                return frequency;
            }
            set
            {
                if (frequency != value)
                {
                    frequency = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Список изменений
        /// </summary>
        public DataChangeCollection Changes { get; set; } = DataChangeCollectionFactory.Create();
        IEnumerable<IDataChange> IDayEventData.Changes => Changes;

        string basedOn;
        /// <summary>
        /// Ключ события - основы
        /// </summary>
        public string BasedOn
        {
            get
            {
                return basedOn;
            }
            set
            {
                if (basedOn != value)
                {
                    basedOn = value;
                    OnPropertyChanged();

                    OnPropertyChanged("Group");
                    OnPropertyChanged("Conditions");
                    OnPropertyChanged("Frequency");
                    OnPropertyChanged("Locs");
                }
            }
        }

        public override LocData Locs {
            get
            {
                if (!string.IsNullOrEmpty(basedOn))
                {
                    try
                    {
                        return DayEventManager.Get(basedOn)?.Locs;
                    }
                    catch { }
                }
                return base.Locs;
            }
        }

        protected override string LocKey => $"Event.{Key}";

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();

            pack.Add(Key);
            pack.Add(Group);
            pack.Add(Conditions);
            pack.Add(Locs.En);
            pack.Add(Locs.Ru);

            return pack;
        }
    }
}
