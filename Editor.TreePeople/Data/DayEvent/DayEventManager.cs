﻿using Editor.Core.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.TreePeople.Data
{
    /// <summary>
    /// Менеджер событий дня
    /// </summary>
    public class DayEventManager : BaseDataManager<DayEventData>
    {
        static Lazy<DayEventManager> instance = new Lazy<DayEventManager>(()=> BaseDataManager.Create<DayEventManager, DayEventData>("DayEvent"), true);

        public override string Name => "События";

        public static DayEventManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            foreach(var item in Items)
            {
                if(item.BasedOn != null)
                {
                    var based = Get(item.BasedOn);
                    if(based != null)
                    {
                        item.Locs.En = based.Locs.En;
                        item.Locs.Ru = based.Locs.Ru;
                    }
                }
            }
            BaseDataManager.Save(this, "DayEvent");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            state.RemoveRecursive("BasedOn");

            ExportHandler.Save("DayEvent", state.ToString(Formatting.Indented));
        }

        protected override void ProceedAdd(DayEventData item)
        {
            base.ProceedAdd(item);
            item.Frequency = 1f;
            if (Items.Count == 0) {
                item.Key = "0";
                return;
            }

            var last = Items[Items.Count - 1];
            if (int.TryParse(last.Key, out int lastIndex))
            {
                item.Key = (lastIndex + 1).ToString();
            }
        }

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        public static DayEventData Get(string key)
        {
            var inst = GetInstance();

            foreach (var item in inst.Items)
            {
                if (item.Key == key)
                {
                    return item;
                }
            }

            return null;
        }
    }
}
