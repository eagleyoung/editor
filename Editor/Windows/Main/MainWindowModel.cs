﻿using System.Threading.Tasks;
using Editor.Update;
using Lib.Core;
using Lib.WPF;

namespace Editor.Windows
{
    public class MainWindowModel : BaseViewModel
    {
        protected override async Task OnInitializedAsync()
        {
            await UpdateManager.Update();

            CoreHelper.StartProcess($"{CoreHelper.GetAppFolder()}/Editor.Launcher.exe");
            OnRequestClose();
        }
    }
}
