﻿using System.Threading.Tasks;

namespace Editor.Update
{
    public static class UpdateManager
    {
        /// <summary>
        /// Обновить редактор
        /// </summary>
        public static async Task Update()
        {
            await Task.Delay(1000);
        }
    }
}
