﻿using Lib.WPF;

namespace Editor.Core.Formula
{
    /// <summary>
    /// Данные для решения формулы
    /// </summary>
    public class FormulaData : NotifyObject
    {
        /// <summary>
        /// Установить ключ формулы
        /// </summary>
        public void SetKey(string _key)
        {

        }

        /// <summary>
        /// Установить формулу
        /// </summary>
        public void Set(string _formula)
        {

        }

        object val;
        /// <summary>
        /// Вычисленное значение формулы
        /// </summary>
        public object Value
        {
            get
            {
                return val;
            }
            private set
            {
                if (val != value) {
                    val = value;
                    OnPropertyChanged("Value");
                }
            }
        }
    }
}
