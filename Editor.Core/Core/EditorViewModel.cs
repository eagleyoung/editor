﻿using Editor.Core.Data;
using Lib.WPF;
using System.Windows;
using System.Windows.Input;

namespace Editor.Core
{
    /// <summary>
    /// Базовая модель представления окна редактора
    /// </summary>
    public class EditorViewModel : BaseViewModel
    {
        /// <summary>
        /// Менеджер данных
        /// </summary>
        public ParamManager Param { get; } = ParamManager.GetInstance();

        /// <summary>
        /// Менеджер перевода
        /// </summary>
        public LocManager Loc { get; } = LocManager.GetInstance();

        public EditorViewModel()
        {
            EventManager.RegisterClassHandler(typeof(Window), UIElement.KeyDownEvent, new RoutedEventHandler(GlobalClick));
        }

        private void GlobalClick(object sender, RoutedEventArgs args)
        {
            var e = (KeyEventArgs)args;
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (e.Key == Key.S)
                {
                    BaseDataManager.SaveAll();
                    Utilities.ShowPopup("Сохранено");
                }
                else if (e.Key == Key.E)
                {
                    BaseDataManager.ExportAll();
                    Utilities.ShowPopup("Данные экспортированы");
                }
            }
        }
    }
}
