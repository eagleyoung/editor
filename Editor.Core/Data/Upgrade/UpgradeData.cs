﻿using DataInterface.Core;
using Lib.WPF;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Editor.Core.Data
{
    /// <summary>
    /// Улучшение
    /// </summary>
    public class UpgradeData : LocalizedDataObject, IUpgradeData
    {
        /// <summary>
        /// Требования для покупки улучшения
        /// </summary>
        public DataRequireCollection Requires { get; set; } = DataRequireCollectionFactory.Create();
        IEnumerable<IDataRequire> IUpgradeData.Requires => Requires;

        /// <summary>
        /// Изменения при покупке улучшения
        /// </summary>
        public DataChangeCollection Changes { get; set; } = DataChangeCollectionFactory.Create();
        IEnumerable<IDataChange> IUpgradeData.Changes => Changes;

        bool repeatable;
        /// <summary>
        /// Возможно повторно покупать улучшение?
        /// </summary>
        public bool Repeatable
        {
            get
            {
                return repeatable;
            }
            set
            {
                if(repeatable != value)
                {
                    repeatable = value;
                    OnPropertyChanged();
                }
            }
        }

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();
            pack.Add(Key);
            pack.Add(Locs.En);
            pack.Add(Locs.Ru);
            return pack;
        }

        protected override string LocKey => $"Upgrade.{Key}";
        
        Lazy<LocData> locDescription;
        [JsonIgnore]
        /// <summary>
        /// Перевод описания объекта
        /// </summary>
        public LocData LocDescription
        {
            get
            {
                return locDescription.Value;
            }
        }

        public override string Key
        {
            get => base.Key;
            set
            {
                base.Key = value;
                LocDescription.Key = $"Upgrade.{Key}.Description";
            }
        }

        public UpgradeData()
        {
            locDescription = new Lazy<LocData>(() => {
                var data = LocManager.Get($"Upgrade.{Key}.Description");
                data.PropertyChanged += LocChanged;
                return data;
            });
        }
    }
}
