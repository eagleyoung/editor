﻿using System.Collections.Generic;
using DataInterface.Core;
using Lib.WPF;

namespace Editor.Core.Data
{
    /// <summary>
    /// Уровень опыта
    /// </summary>
    public class ExpData : LocalizedDataObject, IExpData
    {
        double val;
        /// <summary>
        /// Значение переменной, при котором производится получение уровня
        /// </summary>
        public double Value {
            get
            {
                return val;
            }
            set
            {
                if(val != value)
                {
                    val = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Список изменений
        /// </summary>
        public DataChangeCollection Changes { get; set; } = DataChangeCollectionFactory.Create();
        IEnumerable<IDataChange> IExpData.Changes => Changes;

        protected override string LocKey => $"Exp.{Key}";

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();
            pack.Add(Key);
            return pack;
        }
    }
}
