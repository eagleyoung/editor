﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Editor.Core.Data
{
    /// <summary>
    /// Расширения классов для работы с данными
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Убрать определенные поля из данных Json
        /// </summary>
        public static void RemoveRecursive(this JToken token, string[] fields)
        {
            if (!(token is JContainer container)) return;

            List<JToken> removeList = new List<JToken>();
            foreach (JToken el in container.Children())
            {
                if (el is JProperty p && fields.Contains(p.Name))
                {
                    removeList.Add(el);
                }
                RemoveRecursive(el, fields);
            }

            foreach (JToken el in removeList)
            {
                el.Remove();
            }
        }

        /// <summary>
        /// Убрать определенное поле из данных Json
        /// </summary>
        public static void RemoveRecursive(this JToken token, string field)
        {
            token.RemoveRecursive(new string[] { field });
        }
    }
}
