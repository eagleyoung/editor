﻿using Lib.WPF;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Text;

namespace Editor.Core.Data
{
    /// <summary>
    /// Коллекция изменений данных
    /// </summary>
    public class DataChangeCollection : SafeObservableCollection<DataChange>
    {
        protected override void InsertItem(int index, DataChange item)
        {
            base.InsertItem(index, item);
            item.PropertyChanged += ItemChanged;
        }

        private void ItemChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("IsRelative");
            OnPropertyChanged("Output");
        }

        protected override void RemoveItem(int index)
        {
            var item = this[index];
            item.PropertyChanged -= ItemChanged;
            base.RemoveItem(index);
        }

        [JsonIgnore]
        /// <summary>
        /// Изменения зависят от переменных?
        /// </summary>
        public bool IsRelative
        {
            get
            {
                foreach (var change in this)
                {
                    if (!string.IsNullOrEmpty(change.MultiplierKey))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        [JsonIgnore]
        /// <summary>
        /// Вывод описания изменения одной строкой
        /// </summary>
        public string Output
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                for(int i = 0; i < Count; i++)
                {
                    var item = this[i];
                    if (string.IsNullOrEmpty(item.ParamKey)) continue;

                    var sign = "+";
                    if (item.Value != null && item.Value[0] == '-') sign = "";
                
                    var multiplier = string.IsNullOrEmpty(item.MultiplierKey) ? "" : $" * {item.MultiplierKey}";

                    builder.Append($"{item.ParamKey} > {sign}{item.Value}{multiplier}");
                    if(i < Count - 1)
                    {
                        builder.Append(Environment.NewLine);
                    }
                }
                return builder.ToString();
            }
        }
    }

    public static class DataChangeCollectionFactory
    {
        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static DataChangeCollection Create()
        {
            DataChangeCollection output = null;
            new Action(delegate {
                output = new DataChangeCollection();
            }).UIInvoke();
            return output;
        }
    }
}
