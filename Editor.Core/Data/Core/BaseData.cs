﻿using DataInterface.Core;
using Lib.WPF;

namespace Editor.Core.Data
{
    /// <summary>
    /// Базовый объект данных
    /// </summary>
    public abstract class BaseData : SearchableItem, IBaseData
    {
        string key;
        /// <summary>
        /// Ключ данных
        /// </summary>
        public virtual string Key {
            get
            {
                return key;
            }
            set
            {
                if(key != value)
                {
                    key = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
