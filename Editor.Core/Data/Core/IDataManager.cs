﻿namespace Editor.Core.Data
{
    public interface IDataManager
    {
        void Search(string search);
        void Save();
        void Export();
    }
}
