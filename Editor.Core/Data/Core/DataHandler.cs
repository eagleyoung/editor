﻿namespace Editor.Core.Data
{
    /// <summary>
    /// Класс для управления данными
    /// </summary>
    public static class DataHandler
    {
        /// <summary>
        /// Путь до папки с данными
        /// </summary>
        public static string Path { get; private set; }

        /// <summary>
        /// Установить путь до папки с данными
        /// </summary>
        public static void SetPath(string path)
        {
            Path = path;
        }
    }
}
