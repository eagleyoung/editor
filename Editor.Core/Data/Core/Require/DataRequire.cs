﻿using DataInterface.Core;
using Lib.WPF;

namespace Editor.Core.Data
{
    /// <summary>
    /// Отдельное требование
    /// </summary>
    public class DataRequire : NotifyObject, IDataRequire
    {
        bool isCost;
        /// <summary>
        /// Списывать требование?
        /// </summary>
        public bool IsCost {
            get
            {
                return isCost;
            }
            set
            {
                if(isCost != value)
                {
                    isCost = value;
                    OnPropertyChanged();
                }
            }
        }

        string paramKey;
        /// <summary>
        /// Ключ параметра для проверки
        /// </summary>
        public string ParamKey {
            get
            {
                return paramKey;
            }
            set
            {
                if(paramKey != value)
                {
                    paramKey = value;
                    OnPropertyChanged();
                }
            }
        }

        string val;
        /// <summary>
        /// Требуемое значение
        /// </summary>
        public string Value {
            get
            {
                return val;
            }
            set
            {
                if(val != value)
                {
                    val = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
