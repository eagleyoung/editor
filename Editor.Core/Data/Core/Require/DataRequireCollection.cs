﻿using Lib.WPF;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Text;

namespace Editor.Core.Data
{
    /// <summary>
    /// Коллекция требований данных
    /// </summary>
    public class DataRequireCollection : SafeObservableCollection<DataRequire>
    {
        protected override void InsertItem(int index, DataRequire item)
        {
            base.InsertItem(index, item);
            item.PropertyChanged += ItemChanged;
        }

        private void ItemChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Output");
        }

        protected override void RemoveItem(int index)
        {
            var item = this[index];
            item.PropertyChanged -= ItemChanged;
            base.RemoveItem(index);
        }


        [JsonIgnore]
        /// <summary>
        /// Вывод описания требований одной строкой
        /// </summary>
        public string Output
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < Count; i++)
                {
                    var item = this[i];
                    if (string.IsNullOrEmpty(item.ParamKey)) continue;
                    builder.Append($"{item.ParamKey} > {item.Value}");
                    if (i < Count - 1)
                    {
                        builder.Append(Environment.NewLine);
                    }
                }
                return builder.ToString();
            }
        }
    }

    public static class DataRequireCollectionFactory
    {
        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static DataRequireCollection Create()
        {
            DataRequireCollection output = null;
            new Action(delegate {
                output = new DataRequireCollection();
            }).UIInvoke();
            return output;
        }
    }
}
