﻿using Lib.Core;
using Lib.Shared;
using System;
using System.IO;

namespace Editor.Core.Data
{
    /// <summary>
    /// Объект для управления бэкапами данных
    /// </summary>
    public static class BackupHandler
    {
        /// <summary>
        /// Папка с данными бэкапа
        /// </summary>
        static DirectoryInfo backupFolder;

        /// <summary>
        /// Количество бэкапов файла
        /// </summary>
        static readonly int backupCount;

        static BackupHandler()
        {
            backupCount = Config.Instance.ByKey("Core.DataBackupCount")?.Int ?? 0;

            backupFolder = new DirectoryInfo($"{DataHandler.Path}/GameData/Backup");
            if (!backupFolder.Exists)
            {
                backupFolder.Create();
            }
        }

        /// <summary>
        /// Зарегистрировать файл для бэкапа
        /// </summary>
        public static void Register(string path)
        {
            var info = new FileInfo(path);

            var dir = new DirectoryInfo(Path.Combine(backupFolder.FullName, info.Name));
            if (!dir.Exists)
            {
                dir.Create();
            }

            File.Copy(path, Path.Combine(dir.FullName, DateTime.Now.Writable()), true);

            var files = dir.GetFiles("*", SearchOption.TopDirectoryOnly);
            if(files.Length > backupCount)
            {
                FileInfo top = null;
                foreach(var file in files)
                {
                    if(top == null || top.CreationTime > file.CreationTime)
                    {
                        top = file;
                    }
                }

                if(top != null)
                {
                    top.Delete();
                }
            }
        }
    }
}
