﻿using DataInterface.Core;
using Lib.WPF;

namespace Editor.Core.Data
{
    /// <summary>
    /// Перевод
    /// </summary>
    public class LocData : BaseData, ILocData
    {
        string en;
        /// <summary>
        /// Английский перевод
        /// </summary>
        public string En {
            get
            {
                return en;
            }
            set
            {
                if(en != value)
                {
                    en = value;
                    OnPropertyChanged();
                }
            }
        }

        string ru;
        /// <summary>
        /// Русский перевод
        /// </summary>
        public string Ru {
            get
            {
                return ru;
            }
            set
            {
                if(ru != value)
                {
                    ru = value;
                    OnPropertyChanged();
                }
            }
        }

        bool isExternal;
        /// <summary>
        /// Перевод заполняется в другом редакторе?
        /// </summary>
        public bool IsExternal
        {
            get
            {
                return isExternal;
            }
            set
            {
                if(isExternal != value)
                {
                    isExternal = value;
                    OnPropertyChanged();
                }
            }
        }

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();

            pack.Add(Key);
            pack.Add(En);
            pack.Add(Ru);

            return pack;
        }
    }
}
