﻿using DataInterface.Core;
using Lib.WPF;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Editor.Core.Data
{
    /// <summary>
    /// Уровень игры
    /// </summary>
    public class LevelData : LocalizedDataObject, ILevelData
    {
        /// <summary>
        /// Требования для перехода на уровень
        /// </summary>
        public DataRequireCollection Requires { get; set; } = DataRequireCollectionFactory.Create();
        IEnumerable<IDataRequire> ILevelData.Requires => Requires;

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();
            pack.Add(Key);
            pack.Add(Locs.En);
            pack.Add(Locs.Ru);
            return pack;
        }

        protected override string LocKey => $"Level.{Key}";

        Lazy<LocData> locDescription;
        [JsonIgnore]
        /// <summary>
        /// Перевод описания объекта
        /// </summary>
        public LocData LocDescription
        {
            get
            {
                return locDescription.Value;
            }
        }

        public override string Key
        {
            get => base.Key;
            set
            {
                base.Key = value;
                LocDescription.Key = $"Level.{Key}.Description";
            }
        }

        public LevelData()
        {
            locDescription = new Lazy<LocData>(() => {
                var data = LocManager.Get($"Level.{Key}.Description");
                data.PropertyChanged += LocChanged;
                return data;
            });
        }
    }
}
