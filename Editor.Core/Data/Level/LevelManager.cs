﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер уровней игры
    /// </summary>
    public class LevelManager : BaseDataManager<LevelData>
    {
        static Lazy<LevelManager> instance = new Lazy<LevelManager>(()=>  BaseDataManager.Create<LevelManager, LevelData>("Level"), true);

        public override string Name => "Уровни";

        public static LevelManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Level");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);
            ExportHandler.Save("Level", state.ToString(Formatting.Indented));
        }
    }
}
